     ______                                    ________
    / ____/|                    __            / / ____/|  __    v2.0
   / /|___|/______   __ _______/ /_________  / / /____|/\/ /|   (c) 1994
  / / /    / __/ /| / // _____/ ___/ ___  /|/ / ___/ /|\  / /   by Frank J.
 / /_/____/ / / //_/ /_\___ \/ /|_/ /|_/ /_/ / / _/ / //  \/    Perricone
/________/_/ /____  /_______/____/________/_/_/ //_/ //_/\_\
|________|_|/|__ / /| ______|____|________|_|_|/ |_|/ |_|/|_|   All rights
            ____/ / /                                           reserved
           /_____/ /
           |_____|/   by Frank J. Perricone, 1:325/611, perricone@wsyd.com
                         compiled C version by Brian McMurry, 1:102/844


*** What is CrystalFix? **************************************************

CrystalFix is an Areafix program for your FidoNet BBS.  It works with (and
only with) the TrapToss message tosser by Rene Hexel et. al.  Optionally
it can also be used with FTick 0.97 by Peer Hasselmeyer.  The version in
this archive is in ARexx; a compiled C version is available to registered
users.  (Registration costs one postcard; see below.)

An Areafix program is a program which takes care of attaching and detaching
any other systems which feed from you, from those areas you or your host
carries.  With an Areafix installed, if one of your downstream nodes wants
to start carrying an area, they send a message to Areafix on your system
with commands for text.  Areafix reads the message, carries out the
commands (updating your tosser configuration), and sends a reply to the
SYSOP who made the request.

Using this system, your downlinks can do the following things with
CrystalFix:
 * get a list of areas available to them, which shows which ones they
   are already connected to; this includes both message and file areas
 * attach to an area
 * detach from an area
 * rescan an area (message only)
 * request a message area you don't carry, but which your uplink does.
   The request is automatically forwarded upstream and connected.

All this is fairly typical, more or less, of most Areafix programs.  What
makes CrystalFix special?

 * It works tightly with TrapToss.  In fact, it doesn't even have its own
   config file.  Instead, it uses TrapToss's config file.  It gets almost
   all of its info from your already-present TrapToss items, such as your
   node address, your area list, even your loglevel.  You can add a few
   (optional) extra commands to your AREA command blocks (which won't
   bother TrapToss) to control fixing those areas.  There are also a few
   CrystalFix-specific commands which reside in the [CrystalFix] section.
   One less config file to create, one less file to manage every time you
   make a change on your system.

 * Support for FTick so message and file areas are handled by one program.
   File areas can be attached and detached (with your choice of default
   FTick flags).  Since FTick doesn't support rescans or passthroughs,
   neither does CrystalFix for file areas.

 * Simple setup.  Copy a few files, add a line to your AfterSession
   script, and then add a few config entries to your Mail:Fido.cfg and
   you're all ready.  For FTick support, add basically the same items to
   your FTick areas, plus one new keyword, and you're set.

 * Support for forwarded requests.  You can have a list of areas that
   your host (or hosts) make available to you, which you don't carry.
   Your downlinks can request them, and CrystalFix will automatically
   forward the request upstream and update the config file appropriately.
   If the last downlink detaches from such an area, the detach will also
   be forwarded upstream and the area deleted.  This all happens
   automatically.  You use the FIDONET.NA file or equivalent as the list
   of upstream areas, so that you don't have to edit it, and so new ones
   automatically update your configuration when they come in via file
   echo or whatever.

 * Doesn't leave a million files laying around.  Other than the program
   itself it uses only one file, in Mail:.  If you support upstream
   forwarded requests, it uses external files in the FIDONET.NA
   format as well; this saves you having to import them into your
   config file, and means new .NA files automatically update your
   setup.

 * Each system you authorize to areafix from you gets a security level.
   By protecting individual areas, you can hand out access as you wish.
   Unlike most Areafix programs, this can also be used to FORCE systems
   to get an echo.  Simply attach them manually, then raise the level
   of that echo above their level.  They still get the echo, but they
   can't attach, detach, or rescan it.  Areas can be grouped, and you
   can give access within groups at various levels, and as many groups
   as you like for each system.  Rescan avoids the dupes-to-points
   problem caused by simply deleting the hiwater mark.  (If you don't
   know what that is, don't worry about it.)

 * Areas have descriptions which are displayed in queries and messages,
   whenever appropriate.  Helps your downlinks decide what they want.
   Fill these in straight from FIDONET.NA or equivalent, or write your
   own.  They're entirely optional, too.  Forwarded-request areas get
   their descriptions from the FIDONET.NA file automatically.

 * Full multinetwork support; your nodes can areafix areas in different
   networks.  The only restriction: the message requesting an area must
   be addressed from the address they want the area to be sent to.
   (That sentence will make sense if you read it a few times.)

 * No limit to the number of areas, the number of authorized systems,
   or the number of forward-requestable areas, except available memory.
   No limits to the number of security levels, the lengths of
   descriptions, etc.  No limits on how many systems can be attached to an
   area, except for what TrapToss enforces.

 * 9 logging levels.  Get as terse or as voluminous a report as you like.

 * It's inexpensive -- costs you one postcard plus one stamp.

 * Source is released, in ARexx, for easy modification.  Compiled C
   version, written by Brian McMurry based on the ARexx version written
   by me, will be available to registered users.

 * These docs may not be "fantastic", but at least they're step-by-step.

 and the most important one -- the reason I wrote my own Areafix --

 * unlike several of the others out there, IT WORKS, and it doesn't
   turn your tosser and ticker config files into a tossed salad!


*** What are the disadvantages? ******************************************

 * You have to drive all the way into town to get a postcard!
   Horrors!  :)


*** How do I set up CrystalFix? ******************************************

  "Explain all that", said the Mock Turtle.
  "No, no!  The adventures first!", said the Gryphon in an impatient tone:
  "explanations take such a dreadful time."

  -- Lewis Carroll, _Alice's_Adventures_In_Wonderland_

Note: the following instructions are not a brief overview, they are
the whole ball of wax.  You can do them one at a time without even
reading ahead, and get your setup running in no time.  Please read
each one thoroughly and carefully, though.

 1. Copy the following files to somewhere in your path.  Or better yet,
    for a significant speed increase, make them pure, then make them
    resident in your s:user-startup or somewhere.

      FAreaSize
      FidoSetInfo
      FidoGetInfo
      FidoShow
      FidoEnter

    You may find you need a larger stack than the default 4000.  I am
    not sure exactly how much.  I have lots of memory so I keep mine at
    20000 all the time anyway.  Change this by adding a line to the
    script that runs CrystalFix, or even to your user-startup or
    startup-sequence.  Also, it is assumed TrapToss itself is in your
    path somewhere.

 2. Copy CrystalFix.rexx to REXX: or Mail:rexx or somewhere.  (I use
    Mail:rexx.)  Or if you're using the C compiled version, copy
    CrystalFix to somewhere in your PATH.

 3. Copy CrystalFix.help into MAIL:.  You might want to edit it, but
    it should be perfectly usable as is.  You can put it somewhere
    else if you like.

 4. You will need to make a few changes to your TrapToss config file.
    Most of these will be pretty simple, but we'll go through them
    one at a time.  I assume herein that your config file is named
    Mail:Fido.cfg but it doesn't matter if it's something else.

    In case you don't know, your config file is broken up into
    "sections".  The file starts with the global section, whose options
    apply to ALL programs.  Then you'll find lines with a name inside
    square brackets, like [TrapToss], which mark the beginning of
    sections specific to that program.  You should always have a
    TrapToss section, and you'll be creating a CrystalFix section later.
    You may also have sections for TrapList or other programs.

    The first thing to do is to check on the placement of your AREA
    blocks.  In TrapToss's config file, each AREA command begins a
    block and is followed by zero or more lines modifying that area,
    such as MAIN, KEEP, DISALLOW, STRIP, etc.  The block is ended
    by the next AREA, the end of a section, or the end of file.
    You should never have any "global" (that is, not area-specific)
    commands between or after your AREA blocks.  You can get away
    with it sometimes, but you could confuse TrapToss, and you
    definitely will confuse CrystalFix.  So ensure that the AREA blocks
    are all together, and that they're the last thing in the TrapToss
    section.

    Your file is probably already set up correctly in this respect.
    If not, move the AREA blocks to the end of the TrapToss section.

    One other thing to check is your 4DMSGHEADER command, or lack thereof.
    The default is to not have 4D message headers.  If your BBS can support
    them, I *STRONGLY* suggest you turn them on; it'll make message parsing
    much more reliable.  If your BBS won't support them, ask the author to
    make it support them.  If it's turned off, CrystalFix should still
    work.  (Though messages crossing zones without an INTL kluge will be
    parsed wrong, and other similarly obscure problems can come up.)

    The enclosed SampleFido.cfg file might be worth looking over as you
    read the next few sections.  It consists of extracts from my actual
    config file.

 5. The next step is to plan out your security.  CrystalFix offers two
    tools for security: levels and groups.  Combining them, you can
    control access to a fine degree.  But first you need to learn a bit
    about these tools and then plan out your security.  In the next
    step you'll actually set it up.

    Levels are pretty straightforward: higher numbers mean more
    restrictions, and you can use any number you like.  The default is 0
    for each area.  You can even use negative numbers.  To attach or
    detach an area, you have to have a level as high or higher than
    the area.

    Groups are, in a way, a lot more powerful.  Each group is represented
    by a single letter.  (Actually, you can use digits or punctuation,
    but just stick to letters for now.)  Each downlink system can belong
    to none, one, or several groups.  And each area and host can require
    none, one or several.  The most common thing is for a system to belong
    to one or several, and for an area to require only one.  For example,
    all your Fido echos might require group F, and your local echos might
    require group L.  One of your downlinks might belong to group F, and
    another might have group FL -- that is, both groups.

    If an area requires more than one group, the group letters can occur
    in any order.  And if a system belongs to more than one group, they
    can also occur in any order.  Just as long as the system belongs to
    every group that the area requires, they can get in.

    You can get tricky if you want to.  Suppose you have an area which is
    about the gating between WizardNet (group W) and XylophoneNet (group
    X), and you only want to allow people who have access to *both* nets
    to have access to this area.  Give it a group of "WX".  A system with
    group access "AFXRW" can get this area, but "AFXR" can't, nor can "W".
    To put it simply, to get an area, you have to own all the group letters
    that the area requires.  Note: both areas and systems are allowed to
    list *no* group letters.  An area with no group letters can be accessed
    by anyone (as long as they have a high enough level).

    One more trick: these restrictions only prevent a system from making
    CHANGES to their attach status.  If a system is attached to an area
    but doesn't have security access to that area, they stay attached;
    in fact, they can't DETACH.  This means that not only can you
    prevent a system from getting an area, you can also require them to
    get an area.  In essence, CrystalFix ignores an area that the system
    has no security access to: they can't attach, detach, or rescan it,
    and they won't even see it in queries!  But *TrapToss* doesn't care
    about CrystalFix's levels.

    During this step, plan out your security.  The typical setup uses the
    group letters extensively, but with each area in only one group, and
    uses levels only to set rights within groups.  But you can make many
    combinations.

 6. Now that you've planned your security, you will want to add some
    commands to each AREA block.  That is, add a line or two under each
    AREA command (but before the next AREA command).  I like to indent
    these two spaces, and CrystalFix will also indent them.

    LEVEL <level>
    Use this command to set the level for the preceding AREA.  If you
    leave this out, the level is 0.

    GROUP "<group(s)>"
    You can also use the word GROUPS but GROUP is preferred.  The
    list of groups can be enclosed in quotes; if it's empty, it MUST be.
    If you leave this out, the area requires no groups.  Note: stick
    to letters for your groups, and maybe digits if you run out.  Don't
    use punctuation or other weird characters.  They may work today, but
    they may not work tomorrow.  Also, upper and lower case are
    equivalent.  Spaces will be ignored.

    For example, look at this AREA block:

      AREA TRAPDOOR  Mail:echos/TRAPDOOR    1:325/611.0 1:141/1130.0
        LEVEL 5
        GROUP "F"
        KEEP 100
        DESCRIPTION "Discussion about TrapDoor, TrapToss, et. al."

    (The DESCRIPTION is the next step.)

 7. Put one more line into your AREA blocks with the description of the
    area.  The command is:

      DESCRIPTION "<description>"

    The quotemarks are required if there are any spaces in the
    description; it's best to just put them in.  DESC is a valid (but
    not preferred) equivalent.  If you leave this line out, the area
    will show up as "No description available".  Note: whenever
    CrystalFix has to write the file out, due to any changes, it will
    always write all three of the above commands anyway.

 8. The rest of the configuration commands belong in CrystalFix's
    private section.  Add the line [CrystalFix] to your file.  I
    recommend adding it just after the [TrapToss] section.  (If you
    put it BEFORE the TrapToss section, everything will work fine, but
    any area which you carry and which is also listed in one of your
    host's NA-files (see next section) will be counted twice in the
    count shown to the user, so the count may be off a bit.)

 9. You've finished defining areas that you carry, but CrystalFix will
    let you support areas you don't even carry.  How does it do this?
    With automatic forwarding of requests.  You see, what happens is
    this.  You don't carry the echo FOOBAR, but your host does.  Your
    downlink requests it.  CrystalFix knows that you don't carry it, but
    it also knows your host does, so it creates a new area on your
    system, as a passthrough area (that is, one that just goes through
    your system, without the messages being created for your BBS),
    and then it sends an areafix message to the host requesting the
    area.  The result: transparently, by magic, the downlink gets his
    echo and you don't have to do a thing.

    The same thing happens in reverse.  If there's a passthrough area
    on your system with only one downlink attached, and they detach,
    CrystalFix automatically deletes the area, and sends an areafix
    to the host detaching from it.  Again, you don't have to do a
    thing.

    (If you don't have any of these hosts, or you don't want to support
    forwarded requests, just skip this step.)

    In order for CrystalFix to do this, you have to tell it about your
    host(s).  For each host upstream from you, with whom you have
    areafix priveleges, you need to add a HOST line like this:

      HOST <myaddress> <hostaddress> <hostpswd> <level> "<group>" "<NA-file>"

    <hostaddress> tells the Fido address of the host in question, and
    <hostpswd> is your areafix on that system.  CrystalFix needs to
    know these things in order to create the request that gets passed
    upstream.  <myaddress> is also used to create that request; it's
    the origin on that message.  It's also used in the created area
    as the MAIN address; see the TrapToss manual to see what that
    means.  The simple way to fill in <myaddress>: put your address
    in the network that you're talking to that host on.  If it's your
    AmyNet host, put your AmyNet address.  If it's your Vervan's Net
    host, put your Vervan's Net address.

    The <level> and <group> are used like the individual area levels, but
    apply to all areas that the host carries that you don't.

    Finally, the <NA-file> is the pathname of a file that lists all
    the areas the host carries.  The format of this file is the same
    as that of the FIDONET.NA file:

      <area tag> <description>

    Most networks produce a file in this format, so you can set your
    aftersession up to automatically unpack these files as new ones
    come in, and thus, your forward-request configuration is
    automatically self-updating.  Cool!

    For instance, here's a HOST line for a mythical AmyNet system
    (40:789/123) whose host (40:123/456) has a password set up
    as AMYNET.

      HOST 40:789/123 40:123/456 AMYNET 0 "A" "Mail:AmyNet.NA"

    Optionally, you may want to have some special command executed
    whenever a new area gets created.  You can use this command to update
    BBS setups, notify other downlinks, update cost-keeping databases,
    etc.  If this interests you see the ADDAREACMD in the command
    summary section below.

10. Next, you need to get a list together of all your downlinks, that
    is, all the systems that have the right to areafix from you.
    Give each one of them a password, a security level, and one or more
    security groups (see step 5 for a description of how groups work).  To
    set them up, use the SYSTEM command as follows:

      SYSTEM <address> "<password>" <level> "<groups>"

    For example, if 1:325/611 was allowed to areafix from you and had
    a level of 10 and an areafixing password of XIFAERA, and belonged
    to groups V and F, add this:

      SYSTEM 1:325/611.0 "XIFAERA" 10 "VF"

11. The rest of the options are easy ones.  Anywhere in the file add
    a line like this:

      SYSOP "<sysop name>"

    For instance, mine says

      SYSOP "Frank Perricone"

    This can actually go in the global or TrapToss section as well.
    If you omit this, all mail to you will be to "SYSOP" and mail
    sent upstream will be addressed from "SYSOP".

12. Just like the SYSOP line, but this one is the path of the root
    area where new echos should be created.  TrapToss requires a
    pathname for all areas, even passthroughs, where it never uses
    them.  Just in case it starts using them someday, though,
    CrystalFix creates the area directory.  This is the path to
    create it in.  For example, if yours is Mail:echos (the default)
    and the new echo FOOBAR gets created, the path will be
    Mail:echos/FOOBAR.  The keyword is:

      NEWPATH "Mail:echos"

    This goes in the CrystalFix section and defaults to "Mail:echos".

13. By default, CrystalFix will append to every query a count of the
    number of areas the system is attached to, out of how many
    available, and how many total.  Use the COUNT command to change
    this.  The default is COUNT ALL, which is described above.
    COUNT AVAIL will not show the total number of areas, only
    the available area count.  COUNT ATTACHED will only show the
    number attached.  COUNT NONE will omit the line entirely.
    Note: COUNTS is a synonym for COUNT, and AVAILABLE will work for
    AVAIL.

    The main reason to change this is to save time in startup.  To
    give the total count in ALL, CrystalFix must (during the startup)
    read each host NA file.  If you have any hosts, this can make
    things slow.  By switching this to something other than ALL,
    you'll save that time.  Note: the COUNT command must come before
    the HOST commands if you want to save the time of reading the
    host files.  If it comes after, the display will be right, but the
    time will not be saved after all.

14. The last change to your config.  If you want to tell CrystalFix to
    use a different loglevel than TrapToss, add a line like this:

      LOGLEVEL <loglevel>

    Available loglevels, from 0 to 9, mean the same thing as TrapToss
    loglevels, roughly.  If you leave this out, it will use the same
    level as TrapToss.  Similarly, to change the log file, use:

      LOGFILE "<logfile>"

    By default, CrystalFix will share its log with TrapToss (one less
    log to read).

    If your helpfile isn't named Mail:CrystalFix.help add another line:

      HELPFILE "<helpfile>"

    The default size for tag names and pathnames when rewriting the
    Fido.cfg and Tick.cfg are 23 and 30 respectively.  Change this if you
    like with the following commands:

      TAGLENGTH <number>
      PATHLENGTH <number>

    Now that you're done editing your Fido.cfg file, make a backup.

15. If you use FTick and want to support CrystalFixing your tick areas,
    you will need to also make a few changes to your FTick config.  These
    are almost identical to the same ones you made to the AREA commands
    in your Fido.cfg file.  This time, of course, you could have them under
    FILEECHO or AREA blocks, since FTick supports both.

    Because FTick doesn't support GCF keywords, you have to disguise your
    keywords as comments.  So in each FILEECHO block, you have to add the
    following keywords:

      ;!LEVEL <level>
      ;!GROUP "<group>"
      ;!DESCRIPTION "<description>"

    These work exactly like the equivalent Fido.cfg ones, except for the
    extra semicolon and exclamation point before them.  There's also one
    extra one for FTick:

      ;!FLAGS "<flags>"

    As you know, all areas connected to a fileecho have certain flags
    defining the connection.  For instance, H means that files should
    be placed on hold; * means to never send files to this system;
    etc.  This keyword determines what flags to give to any system
    connecting to this area.  The default, if you don't put this
    keyword in, is "H".  Here's a sample area:

      FILEECHO NODEDIFF                 Mail:Nodelist/
        ;!LEVEL 0
        ;!GROUP "F"
        ;!DESCRIPTION "FidoNet nodelist nodediffs sent down weekly"
        ;!FLAGS "HX"
        TO 1:325/602.0 H*
        Execute "Execute Mail:Scripts/ApplyNodediff %p%n"

    See SampleTick.cfg for more examples.

    Note: you don't have to put any "global" commands like SYSTEM, HOST,
    LOGLEVEL, or any of those, into your tick config.  Those are read once
    in Fido.cfg and used everywhere.

16. Add to your Aftersession script, or wherever you process incoming
    mail, the following line, after the mail is tossed:

      [rx] [<path>]CrystalFix [<fido.cfg-path> [<tick.cfg-path>]]

    The parts in square brackets are optional.  Start off with the
    simplest case:

      rx CrystalFix

    If you didn't put CrystalFix.rexx in REXX:, put the path you actually
    used, before the word CrystalFix, like so:

      rx mail:rexx/CrystalFix

    If your Fido.cfg isn't named Mail:fido.cfg, put the real name after
    the end of the command:

      rx CrystalFix OverThere:Fido.cfg

    If your Tick.cfg isn't named Mail:tick.cfg, put the real name after
    the end of the Fido.cfg name.  In this case, you have to put the
    Fido.cfg-name in even if it's the default Mail:fido.cfg, like so:

      rx CrystalFix Mail:Fido.cfg Mail:tick/tick.cfg

    If you don't support FTick but for some bizarre reason you DO have
    a file named mail:tick.cfg and you don't want it read, you must
    include the tick.cfg name but make it the wrong name, such as

      rx CrystalFix Mail:fido.cfg t:ThisFileIsNotThere

    And if you're using the compiled C version, take the rx off the
    beginning:

      CrystalFix

    I'm afraid that's about as short as you can make it.

17. I recommend that you start making daily backups of your Fido.cfg file
    and Tick.cfg file sometime during your nightly processing.  CrystalFix
    has never scrambled my config file yet, but you can't be too safe.

That's it.  You're ready.


*** How does my downlink request something? ******************************

CrystalFix works almost exactly like any other Areafix.  There are two
exceptions I'll mention later.  If you're not familiar with Areafix
programs, you should read the enclosed CrystalFix.help file now.

The first difference between CrystalFix and other Areafix programs is
that it does not accept the -Q, -L, etc. options after the password on
the subject line.  I don't like them; the more modern %QUERY and similar
commands make a lot more sense.  I don't think this is going to break
any hearts.  Maybe if it does, I'll relent and put those in.  But for now,
I encourage you to encourage your downlinks to get used to the newer, more
powerful syntax.

The second is that CrystalFix's security levels do NOT prevent you from
being attached to an area; they merely prevent you from being able to
change your status with CrystalFix.  Your downlink may be attached (by
you, manually) to an area, but not see it in his %QUERY results, and
not be able to attach, detach, or rescan it.  This is handy for those
areas which are required: network administration echos, test echos,
and the like.  After all, why should they BE ABLE to do something they're
NOT ALLOWED to do?  Keep 'em in line, that's what I say!

Briefly, then, a summary of CrystalFix's operation.  Messages should be
addressed to CrystalFix, Areafix, AreaLink, AreaManager, or GEcho, and
originate from the system in question.  The subject line should be the
system's areafix password only.  (Only the first word is paid attention
to anyway.)  The body text contains commands which can be:

  <areaname> or +<areaname> : attach to an area
  -<areaname>               : detach from an area
  *<areaname>               : rescan an area.  Must already be attached.
  %QUERY                    : send a list of available areas, also showing
                              what areas are already attached.
  %LIST                     : same as %QUERY
  %HELP                     : send a help document

These can occur in any order.  In fact, if you do a %QUERY, then some
attaches, then another %QUERY all in one message, your reply message
will show the query results before the attaches happen, then the
attaches, then the results after the attaches.

Any new commands I decide to add will follow these patterns.

A note on rescan: to avoid dupes, CrystalFix manufacturers an artificial
Fido config file to do the rescan with.  This will look a bit odd in your
log, if you use one log for both programs, as the TrapToss log will be
in the middle of the CrystalFix log.  But it works.


*** Where's the obligatory list of keywords for the config file? *********

Here they are, in more technical verbiage than you've seen before, but
you've already learned about all of them.

  ADDAREACMD "<command>"
    Location: Fido.cfg, CrystalFix section

    Defines a command to be executed whenever a new message area is
    created.  Use the following percent-sign sequences to be automatically
    filled in with the following values:

    %t   new area tag name
    %p   new area path
    %l   security level of new area
    %g   security group of new area
    %d   description of new area

    %m   main address of your system in this area
    %r   requesting system node address
    %s   host system new area is being requested from
    %h   host areafix password

    %f   filename of Fido.cfg
    %o   filename of log file in use
    %v   log level in use
    %y   your (SYSOP) name
    %q   quote mark (because you can't use \" or *" here, sorry)
    %%   %

  COUNT NONE | ATTACHED | AVAIL | ALL
    Location(s): Fido.cfg, CrystalFix section

    Determines how much info to include in the last line of a %QUERY.
    NONE means none.  ATTACHED means only show a count of areas
    attached.  AVAIL shows ATTACHED plus a count of available areas.
    ALL shows all of this plus a count of total areas.  If the level
    is not ALL, the host files will *not* be read when a HOST command
    is encountered, saving some time on systems with big host files.
    COUNTS is a synonym for COUNT, and AVAILABLE is a synonym for
    AVAIL.

  [;!]DESCRIPTION "<description>"
    Location(s): Fido.cfg, TrapToss section, AREA block
                 Tick.cfg, FILEECHO block (with ";!")

    Follows an AREA command, like options like KEEP, MAIN, STRIP, or
    DISALLOW already do.  Sets a description for an area.  If missing,
    the description is "No description available".  Enclose descriptions
    in quotes, and try to keep them to 50 characters or less (though
    there is no real limit).  DESC is a synonym.

  ;!FLAGS "<flags>"
    Location(s): Tick.cfg, FILEECHO block

    Sets the default flags for newly attached systems for this filearea.

  [;!]GROUP "<group>"
    Location(s): Fido.cfg, TrapToss section, AREA block
                 Tick.cfg, FILEECHO block (with ";!")

    As DESCRIPTION, but sets the groups required to get an area.  A system
    must own all the groups listed, if any, to access the area.
    Default is none ("").  GROUPS is a synonym for GROUP.

  HELPFILE "<pathname>"
    Location(s): Fido.cfg, CrystalFix section

    Gives the pathname to an ASCII file used for the %HELP command.
    Default is "Mail:CrystalFix.help".

  HOST <myaddress> <hostaddress> <hostpswd> <level> "<group>" "<NA-file>"
    Location(s): Fido.cfg, CrystalFix section

    Defines a host with whom my system has areafix priveleges, and
    a pathname to a file containing a list of areas on that system.
    <myaddress> is the address to be used in the MAIN config item
    for TrapToss to support multinetwork passthrough areas, and also
    to be put onto areafix forward messages.  <hostaddress> is the
    address of the host; <hostpswd> is your areafix password at that
    host.  <level> & <group> apply to all areas in the list.  <NA-file>
    is the pathname to a file in the format
    <tag> <description>
    for as many lines as you like.  FIDONET.NA is already perfect.
    This must be in the CrystalFix section.  If this isn't after the
    TrapToss section, the counts in %QUERYs may be wrong.

  [;!]LEVEL <level>
    Location(s): Fido.cfg, TrapToss section, AREA block
                 Tick.cfg, FILEECHO block (with ";!")

    As DESCRIPTION, but sets the security level required to access an
    area.  A system must have at least this high a level.  Any number,
    even negatives (or non-integers) is allowed.  Default is 0.

  LOGFILE "<log file>"
    Location(s): Fido.cfg, CrystalFix section

    Pick a file to be used for CrystalFix logging.  If this is not
    present, TrapToss's logfile is used.

  LOGLEVEL <log level>
    Location(s): Fido.cfg, CrystalFix section

    Sets CrystalFix's loglevel.  If this is not present, TrapToss's
    loglevel is used.  The values mean about the same as TrapToss's;
    that is, 1 to 9, 1 is least logging, and 0 is no logging at all.

  NEWPATH "<newpath>"
    Location(s): Fido.cfg, CrystalFix section

    Defines the root area where new echos should be created.  For
    instance, if you use Mail:echos (the default) and a new area with
    a tagname of FOOBIE is created, its path will be Mail:echos/FOOBIE.

  PATHLENGTH <length>
    Location(s): Fido.cfg, CrystalFix section

    Changes the length of pathnames when CrystalFix rewrites Fido.cfg
    and Tick.cfg.  Default is 30 characters.

  SYSOP "<sysop name>"
    Location(s): Fido.cfg, global or TrapToss or CrystalFix section

    Defines the name of the SYSOP, to whom any bounce messages will be
    forwarded.  Default is "SYSOP".

  SYSTEM <address> <password> <level> "<groups>"
    Location(s): Fido.cfg, CrystalFix section

    This keyword defines a system with priveleges to Areafix, the
    password which should appear on the subject lines of its messages,
    and its access level (any number) and groups (see step 5).

  TAGLENGTH <length>
    Location(s): Fido.cfg, CrystalFix section

    Changes the length of tag names when CrystalFix rewrites Fido.cfg
    and Tick.cfg.  Default is 23 characters.

Also note that CrystalFix will only get info from the following TrapToss
commands:

  NODE, NETMAIL, 4DMSGHEADER, LOGFILE, LOGLEVEL, AREA, and MAIN

and these FTick commands:

  FILEECHO, AREA, PATH, TO


*** How do I get support? ************************************************

OK, here's the standard spiel.  CrystalFix is not free.  Close, though.
It's postcardware.  Get a postcard and mail it to me.  Total cost to you
should be less than a dollar, which is not a lot to ask for a program of
this quality, even if you DO have to drive into town to get a postcard.
If you can't find a postcard, take a photo, glue it to some cardstock or a
cutout piece of cereal box, and mail that.  The post office usually will
accept it.  I prefer pics of local scenery of the place where you live.

Mail your postcard to:

  Mynstrel's Song Productions
  RR 2 Box 5239
  Berlin VT 05602

While you're driving all the way into town to get that postcard, get two
or three more.  You might find you like another one of my programs!  :)

The most obvious advantage of registration is that I'll give you access to
the C compiled version, translated to C by Brian McMurry.  This version
should be somewhat faster (though its source is not available, so you can't
tweak it as easily).

In addition, I'll be glad to offer support.  That is, help setting it up,
feature requests, and the like.  Support to non-registered users is not
nearly as assured.  I can be reached in many of the Fido echos, including
AMIGA_SYSOP, TRAPDOOR, and FALCON_CBCS.  Netmail to me at 1:325/611 or
Internet mail at perricone@wsyd.com should work fine too.  If you're
having a repeatable problem, I'd really appreciate a copy of your
Fido.cfg file (before and after, if appropriate).

Please feel free to upload the archive this came in, to your favorite BBS.
And your least-favorite, too.  But don't modify it and distribute the
modified version without my explicit permission.  (Rearchiving it with
a different archiver is allowed as long as all the files inside are the
same.  And I suppose you can add one of those hateful zzzendpad files,
in case there's still some BBS user on a remote island in the Pacific who
hasn't heard of ZModem yet.)  If you want to modify the source code for
your own porpoises, that's fine with me, though it doesn't absolve you of
your responsibility to register it.  But you can't distribute the modified
version without my permission.  (Don't worry, I'm easy.)  And if the
modifications are more than cosmetic, my ability to support the result
may be pretty limited, though I'll do what I can.  The compiled C version
may not be redistributed without the explicit permission of me or Brian.

Registered Users to date:

  USER                  FIDO         BBS               NOTES
  --------------------- ------------ ----------------- ------------------
  Peter Deane           3:622/401    Inquestor         Beta-tester

Be within, not without.

