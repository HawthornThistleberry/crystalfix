/* CrystalFix.rexx  (c) 1994 by Frank J. Perricone.  All rights reserved.

$VER: CrystalFix_v2.0 (c) 1994 by Frank J. Perricone.

*/

address command
options failat 21

parse arg cfgname ticname
cfgname=strip(cfgname)
ticname=strip(ticname)
if cfgname="" then cfgname = "Mail:fido.cfg"
if ticname="" then ticname = "Mail:tick.cfg"
if ~exists(ticname) then ticname=""

maxtaglen=23                    /* max length of tagnames                */
maxpathlen=30                   /* max length of echo area pathnames     */

areas. = ""                     /* list of all areas in system           */
numareas=0

ticks. = ""                     /* list of all fileareas in system       */
numticks=0

systems. = ""                   /* list of all systems with access       */
numsystems=0

hosts. = ""                     /* list of all host systems with files   */
numhosts = 0                    /* number of hosts found                 */
hostareas = 0                   /* number of host areas found            */

dirty = 0                       /* does Fido.cfg need to be rewritten?   */
tickdirty = 0                   /* does Tick.cfg need to be rewritten?   */

helpfile="Mail:CrystalFix.help" /* pathname of help file                 */
logfile="Mail:TrapToss.log"     /* log file, default same as TrapToss    */
logfilsrc=0
loglevel=2                      /* loglevel, default same as TrapToss    */
loglvlsrc=0                     /* set to 1 if Fix LOGLEVEL found        */
defzone=0                       /* default zone or "4D" for 4DMSGHEADER  */
mailpath="Mail:Matrix"          /* netmail path, default as TrapToss(?)  */
sysop="SYSOP"                   /* name of SYSOP, default is SYSOP       */
node="0:0/0.0"                  /* address of this system                */
newpath="Mail:echos/"           /* path to build new echos off of        */
countmode=3                     /* count all areas                       */
newtowrite=0                    /* there are no new areas to write       */
addareacmd=""                   /* command on adding areas               */


/* parse Fido.cfg and build tables ******************************************/
if ~open(fp,cfgname,"r") then do
    say "CrystalFix: Fatal: could not read" cfgname
    exit 20
  end
section="global"
do while ~eof(fp)
  s=strip(translate(readln(fp)," ",'09'x))
  do while right(s,1)="\"
    s=left(s,length(s)-1)||strip(translate(readln(fp)," ",'09'x))
    end
  parse var s s1 s2
  s1=upper(strip(s1))
  s2=strip(s2)
  select
    /* section changing commands ****************************************/
    when s1="[CRYSTALFIX]" then section="crystalfix"
    when s1="[TRAPTOSS]" then section="traptoss"
    when left(s1,1)="[" & s1~="[CRYSTALFIX]" & s1~="[TRAPTOSS]" then section="none"

    /* TrapToss section, general ****************************************/
    when section~="none" & s1="SYSOP" then do
      sysop=compress(s2,'"')
      end
    when (section="traptoss" | section="global") & s1="NODE" then do
      node=word(s2,1)
      node=convertfido("1:0/0.0" node)
      end
    when section="traptoss" & s1="NETMAIL" then do
      mailpath=compress(word(s2,2),'"')
      end
    when section="traptoss" & s1="LOGFILE" then do /* get the logfile pathname */
      if logfilsrc=0 then logfile=compress(s2,'"')
      end
    when section="traptoss" & s1="LOGLEVEL" then do /* get the log level used in TrapToss */
      if loglvlsrc=0 then loglevel=s2
      end
    when section="traptoss" & s1="4DMSGHEADER" then do /* set flag */
      defzone="4D"
      end
    /* TrapToss section, areas ******************************************/
    when section="traptoss" & s1="AREA" then do  /* define an area */
      numareas = numareas + 1
      parse var s2 areas.numareas.tag areas.numareas.path areas.numareas.systems
      areas.numareas.tag = strip(areas.numareas.tag)
      areas.numareas.path = strip(areas.numareas.path)
      areas.numareas.systems = strip(areas.numareas.systems)
      areas.numareas.systems = convertfido(node areas.numareas.systems)
      areas.numareas.level = 0
      areas.numareas.group = ""
      areas.numareas.desc = "No description available"
      areas.numareas.myaddress = ""
      areas.numareas.status = ""
      end
    when section="traptoss" & s1="MAIN" then do
      if numareas=0 then do
          say "CrystalFix: Warning: MAIN keyword without an AREA"
          call addtolog("1 Warning: MAIN keyword without an AREA")
        end; else do
          areas.numareas.myaddress = convertfido(node s2)
        end
      end
    when section="traptoss" & s1="LEVEL" then do
      if numareas=0 then do
          say "CrystalFix: Warning: LEVEL found without preceding AREA"
          call addtolog("1 Warning: LEVEL found without preceding AREA")
        end; else do
          areas.numareas.level = s2
        end
      end
    when section="traptoss" & (s1="GROUP" | s1="GROUPS") then do
      if numareas=0 then do
          say "CrystalFix: Warning: GROUP found without preceding AREA"
          call addtolog("1 Warning: GROUP found without preceding AREA")
        end; else do
          areas.numareas.group = compress(s2,'" ')
        end
      end
    when section="traptoss" & (s1="DESC" | s1="DESCRIPTION") then do
      if numareas=0 then do
          say "CrystalFix: Warning: DESCRIPTION found without preceding AREA"
          call addtolog("1 Warning: DESCRIPTION found without preceding AREA")
        end; else do
          areas.numareas.desc = compress(s2,'"')
          if areas.numareas.desc = "" then areas.numareas.desc="No description available"
        end
      end

    /* CrystalFix section ***********************************************/
    when section="crystalfix" & s1="LOGLEVEL" then do
      loglevel=s2
      loglvlsrc=1
      end
    when section="crystalfix" & s1="LOGFILE" then do
      logfile=compress(s2,'"')
      logfilsrc=1
      end
    when section="crystalfix" & s1="HELPFILE" then do
      helpfile=compress(s2,'"')
      end
    when section="crystalfix" & s1="ADDAREACMD" then do
      addareacmd=compress(s2,'"')
      end
    when section="crystalfix" & s1="TAGLENGTH" then do
      maxtaglen=s2
      end
    when section="crystalfix" & s1="PATHLENGTH" then do
      maxpathlen=s2
      end
    when section="crystalfix" & (s1="COUNT" | s1="COUNTS") then do
      s2=upper(s2)
      select
        when s2="NONE"      then countmode=0
        when s2="ATTACHED"  then countmode=1
        when s2="AVAIL"     then countmode=2
        when s2="AVAILABLE" then countmode=2
        when s2="ALL"       then countmode=3
        otherwise call addtolog("1 Warning: unknown COUNT option" s2)
        end
      end
    when section="crystalfix" & s1="NEWPATH" then do /* path to build new echo paths off of */
      newpath=compress(s2,'"')
      if right(newpath,1)~=":" & right(newpath,1)~="/" then newpath=newpath"/"
      end
    when section="crystalfix" & s1="SYSTEM" then do /* defines a system */
      numsystems = numsystems + 1
      parse var s2 systems.numsystems.address systems.numsystems.password systems.numsystems.level systems.numsystems.group
      systems.numsystems.address = convertfido(node strip(systems.numsystems.address))
      systems.numsystems.password = strip(compress(systems.numsystems.password,'"'))
      systems.numsystems.level = strip(systems.numsystems.level)
      systems.numsystems.group = strip(compress(systems.numsystems.group,'" '))
      end
    when section="crystalfix" & s1="HOST" then do /* define a host with external file */
      numhosts = numhosts + 1
      parse var s2 hosts.numhosts.myaddress hosts.numhosts.hostaddress hosts.numhosts.hostpswd hosts.numhosts.level hosts.numhosts.group hosts.numhosts.filename
      hosts.numhosts.myaddress = convertfido(node strip(hosts.numhosts.myaddress))
      hosts.numhosts.hostaddress = convertfido(node strip(hosts.numhosts.hostaddress))
      hosts.numhosts.hostpswd = strip(compress(hosts.numhosts.hostpswd,'"'))
      hosts.numhosts.level = strip(hosts.numhosts.level)
      hosts.numhosts.group = strip(compress(hosts.numhosts.group,'" '))
      hosts.numhosts.filename = strip(compress(hosts.numhosts.filename,'"'))
      if countmode>=3 then do
          if ~open(hostfp,hosts.numhosts.filename,"r") then do
              say "CrystalFix: Warning: could not open host file" hosts.numhosts.filename
            end; else do
              do while ~eof(hostfp)
                s=strip(translate(readln(hostfp)," ",'09'x))
                parse var s s1 s2
                if s~="" then do
                    if findarea(strip(s1))=0 then hostareas = hostareas + 1
                  end
                end
              call close(hostfp)
            end
        end
      end
    when section="crystalfix" & left(s1,1)~=";" & s1~="" then do
      say "CrystalFix: Warning: unrecognized keyword" s1 "in CrystalFix section"
      end
    otherwise nop /* ignore keywords other than these */
    end
  end
call close(fp)
if defzone=0 then defzone=left(node,pos(":",node)-1)

/* parse Tick.cfg and build tables *********************************************/
if ticname~="" then do
    if ~open(fp,ticname,"r") then do
        say "CrystalFix: Fatal: could not read" ticname
        exit 20
      end
    do while ~eof(fp)
      s=strip(translate(readln(fp)," ",'09'x))
      parse var s s1 s2
      s1=upper(strip(s1))
      s2=strip(s2)
      select
        when s1="AREA" then do
          numticks=numticks+1
          ticks.numticks.tag=s2
          ticks.numticks.path=""
          ticks.numticks.systems=""
          ticks.numticks.level=0
          ticks.numticks.group=""
          ticks.numticks.desc="No description available"
          ticks.numticks.flags="H"
          end
        when s1="PATH" then do
          if numticks=0 then do
              say "CrystalFix: Warning: PATH found without preceding FILEECHO"
              call addtolog("1 Warning: PATH found without preceding FILEECHO")
            end; else do
              ticks.numticks.path=s2
            end
          end
        when s1="FILEECHO" then do
          numticks=numticks+1
          parse var s2 ticks.numticks.tag ticks.numticks.path
          ticks.numticks.tag=strip(ticks.numticks.tag)
          ticks.numticks.path=strip(ticks.numticks.path)
          ticks.numticks.systems=""
          ticks.numticks.level=0
          ticks.numticks.group=""
          ticks.numticks.desc="No description available"
          ticks.numticks.flags="H"
          end
        when s1=";!LEVEL" then do
          if numticks=0 then do
              say "CrystalFix: Warning: LEVEL found without preceding FILEECHO"
              call addtolog("1 Warning: LEVEL found without preceding FILEECHO")
            end; else do
              ticks.numticks.level=s2
            end
          end
        when s1=";!GROUP" | s1=";!GROUPS" then do
          if numticks=0 then do
              say "CrystalFix: Warning: GROUP found without preceding FILEECHO"
              call addtolog("1 Warning: GROUP found without preceding FILEECHO")
            end; else do
              ticks.numticks.group=strip(compress(s2,'"'))
            end
          end
        when s1=";!DESCRIPTION" | s1=";!DESC" then do
          if numticks=0 then do
              say "CrystalFix: Warning: DESCRIPTION found without preceding FILEECHO"
              call addtolog("1 Warning: DESCRIPTION found without preceding FILEECHO")
            end; else do
              ticks.numticks.desc=strip(compress(s2,'"'))
            end
          end
        when s1=";!FLAGS" | s1=";!FLAG" then do
          if numticks=0 then do
              say "CrystalFix: Warning: FLAGS found without preceding FILEECHO"
              call addtolog("1 Warning: FLAGS found without preceding FILEECHO")
            end; else do
              ticks.numticks.flags=strip(compress(s2,'"'))
            end
          end
        when s1="TO" then do
          if numticks=0 then do
              say "CrystalFix: Warning: TO found without preceding FILEECHO"
              call addtolog("1 Warning: TO found without preceding FILEECHO")
            end; else do
              s2=strip(compress(s2,'"'))
              parse var s2 s1 s2
              ticks.numticks.systems=strip(ticks.numticks.systems convertfido(node strip(s1))"="strip(s2))
            end
          end
        otherwise nop
        end
      end
    call close(fp)
  end


/* Summarize table sizes ****************************************************/
if numareas+numticks=0 then do
    say "CrystalFix: Fatal: no areas found!"
    call addtolog("1   FATAL: no areas found")
    exit 10
  end
if numsystems=0 then do
    say "CrystalFix: Fatal: no authorized systems found!"
    call addtolog("1   FATAL: no authorized systems found")
    exit 10
  end

call addtolog("1 CrystalFix started")
call addtolog("5   Found" numareas "listed area(s)")
if ticname~="" then call addtolog("5   Found" numticks "tick area(s)")
if countmode>=3 then call addtolog("5   Found" hostareas "host area(s)")
call addtolog("5   Found" numsystems "authorized system(s)," numhosts "host(s)")

if right(mailpath,1)~="/" & right(mailpath,1)~=":" then mailpath=mailpath"/"


/* scan messages for one to me **********************************************/
"FAreaSize >NIL:" mailpath "CrystalFix"
lo=getenvvar("CrystalFixLowMsg")
hi=getenvvar("CrystalFixHiMsg")
call addtolog("8   Scanning messages from" lo "to" hi)

do msg=lo to hi
  if ~exists(mailpath||msg".msg") then iterate
  "FidoGetInfo >NIL:" mailpath||msg".msg CrystalFix" defzone
  toname=upper(getenvvar("CrystalFixTo"))
  call addtolog("9   checking message" msg "to" toname)
  if getenvvar("CrystalFixLocal")=0 & getenvvar("CrystalFixRecd")=0 & ,
      (toname="AREAFIX" | toname="CRYSTALFIX" | toname="AREALINK" | ,
       toname="GECHO" | toname="AREAMANAGER" | toname="RAID" | toname="ALLFIX") then do
      call addtolog("2   found new Fix message" msg "from" getenvvar("CrystalFixFrom") "at" getenvvar("CrystalFixOrig"))
      "FidoSetInfo >NIL:" mailpath||msg".msg" defzone "RECD 1"
      call addtolog("9     set Received flag on message" msg)
      sysnum=findsystem(convertfido(node getenvvar("CrystalFixOrig")))
      if sysnum~=0 then do /* it's from someone in the table! */
          if upper(word(getenvvar("CrystalFixSubj"),1))~=upper(systems.sysnum.password) then do
              /* password is wrong */
              call addtolog("2     bounced" msg".msg: invalid password")
              call bouncemsg("you supplied an incorrect areafix password.")
            end; else do /* correct password */
              call processmessage /* deal with normal request messages */
            end
        end; else do
          /* it's from an unlisted system */
          call addtolog("2     bounced" msg".msg: system not listed")
          call bouncemsg("you are not allowed to areafix from this system.")
        end
    end
  end
call addtolog("9   Cleaning up after CrystalFix")
"delete >NIL: env:CrystalFix#? t:CrystalFix#?"


/* write out new Fido.cfg if modified ***************************************/
if dirty=1 then do
    call addtolog("3   Writing new" cfgname)
    if ~open(fp,cfgname,"r") then do
        say "CrystalFix: Fatal: could not read" cfgname
        call addtolog("1 FATAL: could not read" cfgname)
        exit 20
      end
    if ~open(out,cfgname".new","w") then do
        say "CrystalFix: Fatal: could not create "cfgname".new"
        call addtolog("1 FATAL: could not create "cfgname".new")
        exit 20
      end
    ungets=""
    section="global"
    do while ~eof(fp)
      if ungets="" then s=trim(translate(readln(fp)," ",'09'x)); else s=ungets
      do while right(s,1)="\"
        s=left(s,length(s)-1)||strip(translate(readln(fp)," ",'09'x))
        end
      ungets=""
      select
        when left(strip(s),1)="[" then do
          if section="traptoss" then do
              call writenewareas
            end
          if upper(strip(s))="[CRYSTALFIX]" then section="crystalfix"
          else if upper(strip(s))="[TRAPTOSS]" then section="traptoss"
          else section="none"
          call writeln(out,s)
          end

        when section="traptoss" & upper(left(strip(s),5))="AREA " then do
          i=findarea(upper(word(s,2)))
          if i=0 then do
              say "CrystalFix: Fatal: an area appeared since I read "cfgname"!"
              call addtolog("1   FATAL: a new area appeared since I last read "cfgname)
              exit 20
            end
          if areas.i.status~="del" then do
              call writeln(out,"AREA "left(areas.i.tag,maxtaglen)"  "left(areas.i.path,maxpathlen)"  "areas.i.systems)
              call writeln(out,"  LEVEL "areas.i.level)
              call writeln(out,'  GROUP "'areas.i.group'"')
              call writeln(out,'  DESCRIPTION "'areas.i.desc'"')
              if areas.i.myaddress ~= "" then call writeln(out,"  MAIN" areas.i.myaddress)
              call addtolog("8     wrote AREA" areas.i.tag)
            end; else do /* don't write the AREA or following lines */
              /* eat MAIN, KEEP etc. lines up to but not including the next AREA */
              s=""
              do while ~eof(fp) & upper(left(strip(s),5))~="AREA " & left(strip(s),1)~="["
                s=strip(translate(readln(fp)," ",'09'x))
                if eof(fp) then do
                    call writenewareas
                    s=""
                  end
                end
              ungets=s
            end
          end

        when section="traptoss" & (upper(left(strip(s),6))="LEVEL " | ,
                                   upper(left(strip(s),6))="GROUP " | ,
                                   upper(left(strip(s),7))="GROUPS " | ,
                                   upper(left(strip(s),5))="MAIN " | ,
                                   upper(left(strip(s),5))="DESC " | ,
                                   upper(left(strip(s),12))="DESCRIPTION ") ,
          then nop /* discard */

        otherwise do
          call writeln(out,s)
          end
        end
      end
    if section="traptoss" then call writenewareas
    call close(fp)
    call close(out)
    call addtolog("5   Replacing" cfgname "with" cfgname".new")
    "delete >NIL: "cfgname".old"
    "rename" cfgname cfgname".old"
    "rename" cfgname".new" cfgname
    dirty=0
  end

/* write out new Tick.cfg if modified ***************************************/
if tickdirty=1 then do
    call addtolog("3   Writing new" ticname)
    if ~open(fp,ticname,"r") then do
        say "CrystalFix: Fatal: could not read" ticname
        call addtolog("1 FATAL: could not read" ticname)
        exit 20
      end
    if ~open(out,ticname".new","w") then do
        say "CrystalFix: Fatal: could not create "ticname".new"
        call addtolog("1 FATAL: could not create "ticname".new")
        exit 20
      end
    do while ~eof(fp)
      s=trim(translate(readln(fp)," ",'09'x))
      select
        when upper(left(strip(s),5))="AREA " | upper(left(strip(s),9))="FILEECHO " then do
          i=findtick(upper(word(s,2)))
          if i=0 then do
              say "CrystalFix: Fatal: an area appeared since I read "ticname"!"
              call addtolog("1   FATAL: a new area appeared since I last read "ticname)
              exit 20
            end
          call writeln(out,"FILEECHO "left(ticks.i.tag,maxtaglen)"  "left(ticks.i.path,maxpathlen))
          call writeln(out,"  ;!LEVEL "ticks.i.level)
          call writeln(out,'  ;!GROUP "'ticks.i.group'"')
          call writeln(out,'  ;!DESCRIPTION "'ticks.i.desc'"')
          call writeln(out,'  ;!FLAGS "'ticks.i.flags'"')
          do j=1 to words(ticks.i.systems)
            call writeln(out,"  TO" translate(word(ticks.i.systems,j)," ","="))
            end
          call addtolog("8     wrote FILEECHO" ticks.i.tag)
          end
        when upper(left(strip(s),8))=";!LEVEL " | ,
             upper(left(strip(s),8))=";!GROUP " | ,
             upper(left(strip(s),9))=";!GROUPS " | ,
             upper(left(strip(s),8))=";!FLAGS " | ,
             upper(left(strip(s),7))=";!FLAG " | ,
             upper(left(strip(s),7))=";!DESC " | ,
             upper(left(strip(s),14))=";!DESCRIPTION " | ,
             upper(left(strip(s),5))="PATH " | ,
             upper(left(strip(s),3))="TO " ,
          then nop /* discard */
        otherwise do
          call writeln(out,s)
          end
        end
      end
    call close(fp)
    call close(out)
    call addtolog("5   Replacing" ticname "with" ticname".new")
    "delete >NIL: "ticname".old"
    "rename" ticname ticname".old"
    "rename" ticname".new" ticname
    tickdirty=0
  end

call addtolog("1   CrystalFix session completed"||'0a'x)
say copies(" ",70)

exit


/****************************************************************************
**
** addtolog
**
** Add a given string to the logfile if the level is low enough
**
*/

addtolog: procedure expose logfile loglevel
  parse arg loglvl ' ' logstr
  call writech(stdout,left(logstr,70)||'0d'x)
  if loglvl>loglevel then return
  if ~open(logfp,logfile,"a") then do
      say "CrystalFix: Warning: could not add logfile entry" logstr "to logfile" logfile
    end; else do
      sd=date('n')
      sd=left(sd,7)||right(sd,2)
      sd=translate(sd,"-"," ")
      s="C" sd time('n')"  "logstr
      call writeln(logfp,s)
      call close(logfp)
    end
  return


/****************************************************************************
**
** findarea
**
** Find a given area in the areas table
**
*/

findarea: procedure expose areas. numareas
  arg areatag
  do i=1 to numareas
    if upper(strip(areas.i.tag))=strip(areatag) then return i
    end
  return 0




/****************************************************************************
**
** findtick
**
** Find a given area in the ticks table
**
*/

findtick: procedure expose ticks. numticks
  arg areatag
  do i=1 to numticks
    if upper(strip(ticks.i.tag))=strip(areatag) then return i
    end
  return 0




/****************************************************************************
**
** findhostarea
**
** Find a given area in the hosts tables; returns the host number,
** and the variables hosttag and hostdesc
**
*/

findhostarea:
  arg areatag
  do ii=1 to numhosts
    if hosts.ii.level > systems.sysnum.level | groupmatch(hosts.ii.group systems.sysnum.group)~=1 then iterate
    if ~open(hostfp,hosts.ii.filename,"r") then do
        say "CrystalFix: Warning: could not open host file" hosts.ii.filename
        call addtolog("1     could not open host file" hosts.ii.filename)
      end; else do
        call addtolog("8     searching host file" hosts.ii.filename "for area" areatag)
        do while ~eof(hostfp)
          hosttag=strip(compress(translate(readln(hostfp)," ",'09'x),'0d'x))
          if hosttag="" then iterate
          parse var hosttag hosttag hostdesc
          if hostdesc = "" then hostdesc="No description available"
          if upper(hosttag)=areatag then do
              call close(hostfp)
              return ii
            end
          end
        call close(hostfp)
      end
    end
  return 0



/****************************************************************************
**
** findsystem
**
** Find a given system in the systems table
**
*/

findsystem: procedure expose systems. numsystems
  arg sysaddress
  do i=1 to numsystems
    if upper(strip(systems.i.address))=strip(sysaddress) then return i
    end
  return 0


/****************************************************************************
**
** getenvvar
**
** Gets the value of an environment variable, or "" if the variable does not
** exist
**
*/

getenvvar: procedure
  arg varname
  if ~open(envfp,"env:"varname,"r") then return ""
  s=readln(envfp)
  call close(envfp)
  return s


/****************************************************************************
**
** bouncemsg
**
** Bounces a message to the sender as well as a copy to the SYSOP, with some
** introductory text giving the reason for the bounce
**
*/

bouncemsg:
  parse arg reason
  "FidoShow >NIL:" mailpath||msg".msg t:CrystalFix.text 0 TEXT"
  if ~open(in,"t:CrystalFix.text","r") then do
      say "CrystalFix: Warning: could not bounce message"
      call addtolog("2   could not read bounce message text")
      return
    end
  if ~open(bounce,"t:CrystalFix.bounce","w") then do
      say "CrystalFix: Warning: could not bounce message"
      call addtolog("2   could not create bounce message text")
      return
    end
  if ~open(tosysop,"t:CrystalFix.tosysop","w") then do
      say "CrystalFix: Warning: could not bounce message"
      call addtolog("2   could not create sysop bounce message text")
      return
    end
  call writeln(tosysop,"CrystalFix bounced this message to" getenvvar("CrystalFixFrom") "at" getenvvar("CrystalFixOrig"))
  call writeln(tosysop,'with this reason: "'reason'"')
  call writeln(tosysop,"")
  call writeln(bounce,"CrystalFix did not process this request because" reason)
  call writeln(bounce,"")
  do while ~eof(in)
    s=readln(in)
    call writeln(bounce,">" s)
    call writeln(tosysop,">" s)
    end
  call close(in)
  call close(bounce)
  call close(tosysop)
  'FidoEnter >NIL:' mailpath 't:CrystalFix.tosysop CrystalFix CrystalFix "'sysop'" "Bounced CrystalFix message" ORIG' node 'PVT KILL DEST' node
  i=getenvvar("CrystalFixMsgNum")
  call linkmsgs(msg i)
  'FidoEnter >NIL:' mailpath 't:CrystalFix.bounce  CrystalFix CrystalFix "'getenvvar("CrystalFixFrom")'" "Bounced CrystalFix message" ORIG' node 'PVT KILL DEST' getenvvar("CrystalFixOrig")
  call linkmsgs(i getenvvar("CrystalFixMsgNum"))
  "delete >NIL: t:CrystalFix.text#? t:CrystalFix.bounce t:CrystalFix.tosysop"
  return


/****************************************************************************
**
** processmessage
**
** Reads and processes a normal Areafix message from a system
**
*/

processmessage:
  /* get the text of the message into t:CrystalFix.text */
  "FidoShow >NIL:" mailpath||msg".msg t:CrystalFix.text 0 TEXT"
  if ~open(in,"t:CrystalFix.text","r") then do
      say "CrystalFix: Warning: could not read message"
      call addtolog("2     could not read message text")
      return
    end
  /* create the text of a reply message */
  if ~open(reply,"t:CrystalFix.reply","w") then do
      say "CrystalFix: Warning: could not create reply"
      call addtolog("2     could not create reply message text")
      return
    end
  call addtolog("7     writing reply")
  call writeln(reply,"Results of your CrystalFix session:")
  call writeln(reply,"")
  do while ~eof(in)
    s=upper(word(readln(in),1)) /* only look at the first word */
    call addtolog("6     read command" s)
    if left(s,1)="+" then s=substr(s,2) /* strip leading + */
    if left(s,3)="[+]" then s=substr(s,4) /* alternative style for dummies */
    select
      when left(s,3)="---" then break /* tearline ends the message */
      when s="" then iterate /* ignore blank lines */
      when s="%QUERY" | s="%LIST" then do
        call addtolog("3     caller queried area list")
        call writeln(reply,"")
        call writeln(reply,"The following areas are available (* indicates you are attached):")
        call writeln(reply,"")
        att=0
        avail=0
        do i=1 to numareas
          if areas.i.level <= systems.sysnum.level & groupmatch(areas.i.group systems.sysnum.group)=1 then do
              call addtolog("8     listing area" areas.i.tag)
              avail=avail+1
              if pos(systems.sysnum.address,areas.i.systems)~=0 then do
                  s="* "
                  att=att+1
                end; else do
                  s="  "
                end
              s=s||left(areas.i.tag,maxtaglen)
              s=s||left(areas.i.desc,73-maxtaglen)
              call writeln(reply,strip(s,'t'))
            end
          end
        do i=1 to numhosts
          if hosts.i.level <= systems.sysnum.level & groupmatch(hosts.i.group systems.sysnum.group)=1 then do
              if ~open(hostfp,hosts.i.filename,"r") then do
                  say "CrystalFix: Warning: could not open host file" hosts.i.filename
                  call addtolog("1     could not open host file" hosts.i.filename)
                end; else do
                  do while ~eof(hostfp)
                    s=strip(compress(translate(readln(hostfp)," ",'09'x),'0d'x))
                    if s="" then iterate
                    parse var s s1 s2
                    if findarea(strip(s1))=0 then do
                        call addtolog("8     listing hostarea" strip(s1))
                        avail = avail + 1
                        call writeln(reply,"  "left(strip(s1),maxtaglen)||left(strip(s2),70-maxtaglen-length(hosts.i.hostaddress))||" ("hosts.i.hostaddress")")
                      end
                    end
                  call close(hostfp)
                end
            end
          end
        do i=1 to numticks
          if ticks.i.level <= systems.sysnum.level & groupmatch(ticks.i.group systems.sysnum.group)=1 then do
              call addtolog("8     listing filearea" ticks.i.tag)
              avail=avail+1
              if pos(systems.sysnum.address,ticks.i.systems)~=0 then do
                  s="* "
                  att=att+1
                end; else do
                  s="  "
                end
              s=s||left(ticks.i.tag,maxtaglen)
              s=s||left(ticks.i.desc,66-maxtaglen)" (FILE)"
              call writeln(reply,strip(s,'t'))
            end
          end
        call writeln(reply,"")
        s="You are attached to" att "echos"
        select
          when countmode=0 then s=""
          when countmode=1 then s=s||"."
          when countmode=2 then s=s||" out of" avail" available to you."
          when countmode=3 then s=s||" out of" avail "available to you," numareas+hostareas+numticks "total."
          otherwise s=s||"."
          end
        call writeln(reply,s)
        call writeln(reply,"")
        call addtolog("5     caller attached to" att "of" avail "available")
        end
      when s="%HELP" then do
        call addtolog("4     caller got help")
        call writeln(reply,"")
        if ~open(help,helpfile,"r") then do
            call writeln(reply,"Sorry, no help is available at this time.")
            say "CrystalFix: Warning: could not read help"
            call addtolog("2     could not read help")
            iterate
          end
        do while ~eof(help)
          call writeln(reply,readln(help))
          end
        call close(help)
        call writeln(reply,"")
        end
      when left(s,1)="-" then do /* detach from area */
        s=substr(s,2)
        call validatearea("detach")
        select
          when valid=0 then nop
          when valid=1 then do
              call detachmsgarea
            end
          when valid=2 then do
              call detachfilearea
            end
          end
        end
      when left(s,1)="*" then do
        s=substr(s,2)
        call validatearea("rescan")
        select
          when valid=0 then nop
          when valid=2 then do
            call writeln(reply,"Filearea" s "not rescanned--you can't rescan file areas")
            call addtolog("4     can't rescan" s", filearea")
            end
          when pos(systems.sysnum.address,areas.areanum.systems)=0 then do
            call writeln(reply,"Not attached to area" s", can't rescan"
            call addtolog("4     can't rescan" s", not attached")
            end
          when areas.areanum.myaddress ~= "" then do
            call writeln(reply,"Area" s" is pass-through, can't rescan"
            call addtolog("4     can't rescan" s", passthrough")
            end
          otherwise do
            call writeln(reply,"Rescanned area" s "("areas.areanum.desc")")
            call addtolog("2     rescanned" s)
            /* "delete >NIL:" areas.areanum.path"/1.msg" */ /* delete hiwater */
            call rescanarea
            end
          end
        end
      otherwise do
        call validatearea("attach")
        select
          when valid=0 then nop
          when valid=1 then do
            if pos(systems.sysnum.address,areas.areanum.systems)~=0 then do
                call writeln(reply,"Already attached to area" s "("areas.areanum.desc")")
                call addtolog("4     already attached to" s)
              end; else do
                call writeln(reply,"Attached to area" s "("areas.areanum.desc")")
                call addtolog("2     attached to" s)
                dirty=1
                areas.areanum.systems = areas.areanum.systems systems.sysnum.address
              end
            end
          when valid=2 then do
            if pos(systems.sysnum.address,ticks.areanum.systems)~=0 then do
                call writeln(reply,"Already attached to filearea" s "("ticks.areanum.desc")")
                call addtolog("4     already attached to" s)
              end; else do
                call writeln(reply,"Attached to filearea" s "("ticks.areanum.desc")")
                call addtolog("2     attached to" s)
                tickdirty=1
                ticks.areanum.systems = ticks.areanum.systems systems.sysnum.address"="ticks.areanum.flags
              end
            end
          end
        end
      end
    end
  call close(in)
  call addtolog("7     finishing up reply")
  call writeln(reply,"")
  call writeln(reply,"Thanks for using CrystalFix.")
  call close(reply)
  call addtolog("8     creating reply message")
  'FidoEnter >NIL:' mailpath 't:CrystalFix.reply CrystalFix CrystalFix "'getenvvar("CrystalFixFrom")'" "Results of CrystalFix session" ORIG' node 'PVT KILL DEST' getenvvar("CrystalFixOrig")
  call linkmsgs(msg getenvvar("CrystalFixMsgNum"))
  "delete >NIL: t:CrystalFix.#?"
  return


/****************************************************************************
**
** validatearea
**
** Make sure a given area is listed, and the system has access to change it;
** sets valid=0 if not valid, valid=1 for message areas (hostareas are
** created here), and valid=2 for file areas
**
*/

validatearea:
  parse arg action
  valid=0
  areanum=findarea(s)
  if areanum~=0 then do /* found a message area, try to attach */
      if areas.areanum.level <= systems.sysnum.level & groupmatch(areas.areanum.group systems.sysnum.group)=1 then do
          call addtolog("7     validated area for" action)
          valid=1
          return
        end; else do
          call writeln(reply,"Insufficient access to" action "area" s "("areas.areanum.desc")")
          call addtolog("4     insufficient access to" action s)
          return
        end
    end
  areanum=findtick(s)
  if areanum~=0 then do /* found a file area, try to attach */
      if ticks.areanum.level <= systems.sysnum.level & groupmatch(ticks.areanum.group systems.sysnum.group)=1 then do
          call addtolog("7     validated fileecho for" action)
          valid=2
          return
        end; else do
          call writeln(reply,"Insufficient access to" action "fileecho" s "("ticks.areanum.desc")")
          call addtolog("4     insufficient access to" action s)
          return
        end
    end
  if action="attach" then do /* search for a host area */
      i=findhostarea(s)
      if i~=0 then do
          call addtolog("3     created HOST area" s "from host file" hosts.i.filename)
          numareas = numareas + 1
          areas.numareas.status = "new"
          areas.numareas.tag = s
          areas.numareas.path = newpath||s
          if ~exists(areas.numareas.path) then "makedir >NIL:" areas.numareas.path
          areas.numareas.systems = hosts.i.hostaddress
          areas.numareas.level = hosts.i.level
          areas.numareas.group = hosts.i.group
          areas.numareas.desc = strip(hostdesc)
          areas.numareas.myaddress = hosts.i.myaddress
          areanum=numareas
          call close(hostfp)
          /* create upstream request message */
          if ~open(upstream,"t:CrystalFix.upstream","w") then do
              say "CrystalFix: Warning: could not create upstream request"
              call addtolog("2     could not create upstream request text")
              return
            end
          call writeln(upstream,s)
          call close(upstream)
          'FidoEnter >NIL:' mailpath 't:CrystalFix.upstream CrystalFix "'sysop'" Areafix' hosts.i.hostpswd 'ORIG' hosts.i.myaddress 'DEST' hosts.i.hostaddress 'PVT KILL'
          call writeln(reply,"Forwarded request to" hosts.i.hostaddress "for area" s "("areas.areanum.desc")")
          call addtolog("2     forwarded request for" s "to" hosts.i.hostaddress)
          /* run add area command */
          if addareacmd~="" then do
              s=substitute("%t" areas.numareas.tag addareacmd)
              s=substitute("%r" systems.sysnum.address s)
              s=substitute("%p" areas.numareas.path s)
              s=substitute("%s" areas.numareas.systems s)
              s=substitute("%l" areas.numareas.level s)
              s=substitute("%g" areas.numareas.group s)
              s=substitute("%d" areas.numareas.desc s)
              s=substitute("%m" areas.numareas.myaddress s)
              s=substitute("%h" areas.numareas.hostpswd s)
              s=substitute("%f" cfgname s)
              s=substitute("%o" logfile s)
              s=substitute("%v" loglevel s)
              s=substitute("%y" sysop s)
              s=substitute("%q" '"' s)
              s=substitute("%%" "%" s)
              ''s
              call addtolog("3     executed" s)
            end
          valid=1
          newtowrite=1
          return
        end
    end
  call writeln(reply,"Couldn't find area" s)
  call addtolog("4     couldn't find requested area" s)
  return




/****************************************************************************
**
** detachmsgarea
**
** Detach a message area
**
*/

detachmsgarea:
  if pos(systems.sysnum.address,areas.areanum.systems)=0 then do
      call writeln(reply,"Not attached to area" s", can't detach")
      call addtolog("4     can't detach" s", not attached")
    end; else do
      call writeln(reply,"Detached from area" s "("areas.areanum.desc")")
      call addtolog("2     detached" s)
      i=pos(systems.sysnum.address,areas.areanum.systems)
      j=pos(" ",areas.areanum.systems,i)
      select
        when i=1 & j=0 then do
          say "CrystalFix: Warning: deleted last system in area" s
          call addtolog("1    WARNING: deleted last system in area" s)
          areas.areanum.systems=""
          end
        when i=1 then do
          areas.areanum.systems=substr(areas.areanum.systems,j+1)
          end
        when j=0 then do
          areas.areanum.systems=left(areas.areanum.systems,i-1)
          end
        otherwise do
          areas.areanum.systems=left(areas.areanum.systems,i-1)||substr(areas.areanum.systems,j+1)
          end
        end
      if words(areas.areanum.systems)<=1 then do /* last system detached from a passthrough */
          call addtolog("2     last system in passthrough detached")
          areas.areanum.status="del"
          call addtolog("6       deleting area directory")
          "delete >NIL:" areas.areanum.path "ALL"
          /* figure out which host to send a message to first */
          i=findhostarea(s)
          if i=0 then do
              say "CrystalFix: Warning: host area not found, can't forward detach"
              call addtolog("1    could not find host to detach area" s)
            end; else do
              call addtolog("6       detaching at host")
              if ~open(upstream,"t:CrystalFix.upstream","w") then do
                  say "CrystalFix: Warning: could not create upstream request"
                  call addtolog("2     could not create upstream request text")
                  return
                end
              call writeln(upstream,"-"s)
              call close(upstream)
              'FidoEnter >NIL:' mailpath 't:CrystalFix.upstream CrystalFix "'sysop'" Areafix' hosts.i.hostpswd 'ORIG' hosts.i.myaddress 'DEST' hosts.i.hostaddress 'PVT KILL'
            end
        end
      dirty=1
    end
  return


/****************************************************************************
**
** detachfilearea
**
** Detach a file area
**
*/

detachfilearea:
  if pos(systems.sysnum.address,ticks.areanum.systems)=0 then do
      call writeln(reply,"Not attached to filearea" s", can't detach")
      call addtolog("4     can't detach" s", not attached")
    end; else do
      call writeln(reply,"Detached from area" s "("ticks.areanum.desc")")
      call addtolog("2     detached" s)
      i=pos(systems.sysnum.address,ticks.areanum.systems)
      j=pos(" ",ticks.areanum.systems,i)
      select
        when i=1 & j=0 then do
          say "CrystalFix: Warning: deleted last system in filearea" s
          call addtolog("1    WARNING: deleted last system in filearea" s)
          ticks.areanum.systems=""
          end
        when i=1 then do
          ticks.areanum.systems=substr(ticks.areanum.systems,j+1)
          end
        when j=0 then do
          ticks.areanum.systems=left(ticks.areanum.systems,i-1)
          end
        otherwise do
          ticks.areanum.systems=left(ticks.areanum.systems,i-1)||substr(ticks.areanum.systems,j+1)
          end
        end
      tickdirty=1
    end
  return




/****************************************************************************
**
** linkmsgs
**
** links a pair of messages together
**
*/

linkmsgs:
  parse arg m1 m2 .
  call addtolog("8     linking message" m1 "to" m2)
  "FidoSetInfo >NIL:" mailpath||m1".msg" defzone "+THREAD" m2
  "FidoSetInfo >NIL:" mailpath||m2".msg" defzone "-THREAD" m1
  return



/****************************************************************************
**
** writenewareas
**
** write out all new areas
**
*/

writenewareas:
  /* new areas */
  if newtowrite=0 then return
  do i=1 to numareas
    if areas.i.status="new" then do
        call writeln(out,"AREA "left(areas.i.tag,maxtaglen)"  "left(areas.i.path,maxpathlen)"  "areas.i.systems)
        call writeln(out,"  LEVEL "areas.i.level)
        call writeln(out,'  GROUP "'areas.i.group'"')
        call writeln(out,'  DESCRIPTION "'areas.i.desc'"')
        if areas.i.myaddress ~= "" then call writeln(out,"  MAIN" areas.i.myaddress)
        call addtolog("8     wrote new AREA" areas.i.tag)
      end
    end
  call writeln(out,"")
  newtowrite=0 /* don't write these again! */
  return


/****************************************************************************
**
** groupmatch
**
** determines if a given group requirement is met
**
*/

groupmatch: procedure
  arg reqd owned
  do i=1 to length(reqd)
    if pos(substr(reqd,i,1),owned)=0 then return 0
    end
  return 1


/****************************************************************************
**
** rescanarea
**
** writes a temporary config file for rescanning a single area
**
*/

rescanarea:
  "delete >NIL:" areas.areanum.path"/1.msg" /* delete hiwater */
  if ~open(rescan,"t:Rescan.cfg","w") then do
      call addtolog("1 Warning: unable to create rescan config t:Rescan.cfg")
      return
    end
  if ~open(fp,cfgname,"r") then do
      call addtolog("1 Warning: could not read" cfgname "to rescan")
      call close(rescan)
      return
    end
  s=strip(translate(readln(fp)," ",'09'x))
  do while upper(left(strip(s),5))~="AREA "
    if eof(fp) then do
        call addtolog("1 Warning: unable to parse" cfgname "to rescan")
        call close(rescan)
        call close(fp)
        return
      end
    call writeln(rescan,s)
    s=readln(fp)
    end
  call close(fp)
  call writeln(rescan,"AREA "left(areas.areanum.tag,maxtaglen)"  "left(areas.areanum.path,maxpathlen)"  "node" "systems.sysnum.address)
  call close(rescan)
  call addtolog("2   rescanning --------------------"||'0a'x)
  "TrapToss >NIL: CONFIG t:Rescan.cfg SCAN NOMATRIX IGNORESENT"
  call addtolog("2   rescan complete ---------------")
  "delete >NIL: t:Rescan.cfg"
  return


/****************************************************************************
**
** convertfido
**
** converts Fido addresses to full 4D using defaults in the first param
** (which MUST be full 4D)
**
*/

convertfido: procedure
  arg default s
  parse var default fido.zone ":" fido.net "/" fido.node "." fido.point
  s=strip(s)
  r=""
  do i=1 to words(s)
    s2=word(s,i)
    j=pos(":",s2)
    if j~=0 then do /* zone is present */
        fido.zone=left(s2,j-1)
        s2=substr(s2,j+1)
      end
    j=pos(".",s2)
    if j~=0 then do /* point is present */
        fido.point=substr(s2,j+1)
        s2=left(s2,j-1)
      end; else do
        if pos("/",s2)~=0 then fido.point=0
      end
    j=pos("/",s2)
    if j~=0 then do /* net is present */
        fido.net=left(s2,j-1)
        s2=substr(s2,j+1)
      end
    if s2~="" then fido.node=s2
    r=r fido.zone":"fido.net"/"fido.node"."fido.point
    end
  r=strip(r)
  return r


/****************************************************************************
**
** substitute
**
** replaces a substring with another substring wherever found
**
*/

substitute: procedure
  parse arg old new s
  old=strip(old)
  new=strip(new)
  s=strip(s)

  i=pos(old,s)
  do while i~=0
    s=left(s,i-1)||new||substr(s,i+length(old))
    i=pos(old,s)
    end
  return s

